package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	
	private int[] rolls = new int[20];
	private int currentRoll = 0;
	
	@Override
	public void roll(int pins) {
		rolls[currentRoll++] = pins;
	}

	@Override
	public int score() {
		int score = 0;
		for (int i=0; i<20; i++) {
			score += rolls[i];	
		}
		return score;
	}

}
